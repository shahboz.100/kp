# Курсовая работа по теме: Проектирование базы данных «Магазин бытовой техники».

## Ссылка на проект: https://i.shahboz.ru/
## Ссылка на панель управления БД http://pgsql.shahboz.ru/

1. Веб сервер nginx + apache
2. Оперционная система Centos 7
2. Фреймворк symfony 5.0.8 (https://symfony.com/)
2. ORM doctrine 2.6 | Идет по умолчанию с symfony (https://www.doctrine-project.org/)
3. PHP 7.4.6
4. Bootstrap 4
5. Postgresql 12 | ссылка на pgadmin (https://pgsql.shahboz.ru/)
4. XDEBUG включен
5. APCu включен
6. OPcache включен
7. Правило оформления комитов (https://habr.com/ru/post/183646/)
8. Ссылка на проект (https://i.shahboz.ru)
