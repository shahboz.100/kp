<?php

namespace App\Form;

use App\Entity\Permission;
use App\Entity\Role;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RoleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Системное имя',
                'attr' => [
                    'placeholder' => 'Введите системное имя...',
                ],
                'help' => 'Обязательное поле',
                'required' => true
            ])
            ->add('visibleName', TextType::class, [
                'label' => 'Отображаемое имя',
                'attr' => [
                    'placeholder' => 'Введите отображаемое имя...',
                ],
                'help' => 'Обязательное поле',
                'required' => true
            ])
            ->add('permissions', EntityType::class, [
                'required' => false,
                'label' => 'Разрешения',
                'class' => Permission::class,
                'placeholder' => 'Выберите разрешения...',
                'by_reference' => false,
                'multiple' => true,
            ])
            ->add('save', SubmitType::class, [
                'attr' => [
                    'hidden' => true
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Role::class,
        ]);
    }
}