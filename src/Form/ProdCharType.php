<?php


namespace App\Form;


use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query\ResultSetMapping;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProdCharType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var EntityManager $em */
        $em = $options['em'];
        $builder
            ->add('category', ChoiceType::class, [
                'label' => 'Характеристика',
                'required' => true
            ])
            ->add('value', TextType::class, [
                'label' => 'Значение',
                'required' => true
            ]);

        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) use ($em) {
            $data = $event->getData();
            $form = $event->getForm();

            $options = $form->get('category')->getConfig()->getOptions();

            if (isset($data['category']) && !empty($data['category'])) {
                $options['choices'] = $this->getChoice($em, $data['category']);
                $form->add('category', ChoiceType::class, $options);
            }
        });

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) use ($em) {
            $data = $event->getData();
            $form = $event->getForm();

            $options = $form->get('category')->getConfig()->getOptions();

            if (isset($data['id_catchar']) && !empty($data['id_catchar'])) {
                $options['choices'] = $this->getChoice($em, $data['id_catchar']);
                $form->add('category', ChoiceType::class, $options);
            }
        });
    }

    /**
     * @param EntityManager $em
     * @param               $category_id
     *
     * @return mixed
     */
    public function getChoice($em, $category_id)
    {
        $rsm = new ResultSetMapping();
        $rsm->addScalarResult('id_catchar', 'id');
        $rsm->addScalarResult('name', 'name');

        $sql = 'select cc.id_catchar, c.name from category_characteristic cc 
                join characteristic c 
                    on cc.caracteristic_id = c.id_characteristic 
                where cc.id_catchar = :category_id';

        $query = $em->createNativeQuery($sql, $rsm);
        $query->setParameter('category_id', $category_id);

        return array_flip(array_column($query->getArrayResult(), 'name', 'id'));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => null,
            'em' => false
        ]);
    }
}