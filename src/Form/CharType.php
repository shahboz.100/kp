<?php


namespace App\Form;


use App\Entity\Characteristic;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CharType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Название характеристики',
                'attr' => [
                    'placeholder' => 'Название',
                ],
                'help' => 'Обязательное поле',
                'required' => true
            ])
            ->add('parent', EntityType::class, [
                'label' => 'Родитель',
                'class' => Characteristic::class,
                'choice_label' => 'name',
                'required' => false
            ])
            ->add('save', SubmitType::class, [
                'attr' => [
                    'hidden' => true
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Characteristic::class,
        ]);
    }
}