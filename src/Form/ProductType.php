<?php


namespace App\Form;


use App\Entity\Category;
use App\Entity\Product;
use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var EntityManager $em */
        $em = $options['em'];
        $data = $options['characteristics'] ?? null;
        $builder
            ->add('name', TextType::class, [
                'label' => 'Название',
                'required' => true
            ])
            ->add('category', EntityType::class, [
                'label' => 'Категория',
                'required' => true,
                'class' => Category::class,
                'choice_label' => 'name'
            ])
            ->add('price', IntegerType::class, [
                'label' => 'Цена',
                'required' => true
            ])
            ->add('characteristic', CollectionType::class, [
                'label' => false,
                'entry_type' => ProdCharType::class,
                'mapped' => false,
                'allow_add' => true,
                'allow_delete' => true,
                'data' => $data,
                'entry_options' => [
                    'em' => $em
                ]
            ])
            ->add('images', FileType::class, [
                'label' => 'Изображение',
                'attr' => [
                    'type' => 'file',
                    'accept' => 'image/*',
                ],
                'multiple' => true,
                'help' => 'Рекомендуемые пропорции изображения 5х2. Можно загружать несколько фото одновременно',
                'required' => false
            ])
            ->add('count', IntegerType::class, [
                'label' => 'Количество',
                'required' => true
            ])
            ->add('description', TextareaType::class, [
                'label' => 'Описание',
                'required' => false
            ])
            ->add('save', SubmitType::class, [
                'label' => 'Сохранить'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Product::class,
            'em' => false,
            'characteristics' => null
        ]);
    }


}