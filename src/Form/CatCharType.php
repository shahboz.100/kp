<?php


namespace App\Form;


use App\Entity\CatChar;
use App\Entity\Category;
use App\Entity\Characteristic;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CatCharType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('category', EntityType::class, [
                'label' => 'Категория',
                'required' => true,
                'class' => Category::class,
                'choice_label' => 'name'
            ])
            ->add('characteristic', EntityType::class, [
                'label' => 'Характеристика',
                'required' => true,
                'class' => Characteristic::class,
                'choice_label' => 'name'
            ])
            ->add('save', SubmitType::class, [
                'label' => 'Сохранить'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CatChar::class
        ]);
    }

}