<?php


namespace App\DataFixtures;


use App\Entity\Role;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class RoleFixtures extends Fixture implements OrderedFixtureInterface
{

    public function load(ObjectManager $manager)
    {
        $role_admin = new Role();
        $role_admin->setName('ROLE_ADMIN');
        $role_admin->setVisibleName('Администратор');

        $manager->persist($role_admin);
        $manager->flush();
        $this->setReference('role_admin', $role_admin);
    }

    public function getOrder()
    {
        return 2;
    }
}