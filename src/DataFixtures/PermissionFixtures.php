<?php


namespace App\DataFixtures;


use App\Controller\PermissionController;
use App\Controller\RolesController;
use App\Controller\UsersController;
use App\Entity\Permission;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Authorization\Voter\AuthenticatedVoter;

class PermissionFixtures extends Fixture implements OrderedFixtureInterface
{

    public function load(ObjectManager $manager)
    {
        $perm_anonymously = new Permission();
        $perm_anonymously->setName(AuthenticatedVoter::IS_AUTHENTICATED_ANONYMOUSLY);
        $perm_anonymously->setVisibleName("Просмотр всеми пользователями");
        $perm_anonymously->setDescription("Доступ всем пользователям");

        $perm_only_anonymously = new Permission();
        $perm_only_anonymously->setName('IS_AUTHENTICATED_ONLY_ANONYMOUSLY');
        $perm_only_anonymously->setVisibleName("Просмотр только гостями");
        $perm_only_anonymously->setDescription("Доступ пользователям не прошедшим процедуру аунтификации");

        $perm_roles = new Permission();
        $perm_roles->setName(RolesController::VIEW);
        $perm_roles->setVisibleName("Просмотр ролей");
        $perm_roles->setDescription("Доступ на открытие и просмотр приложения");

        $perm_users = new Permission();
        $perm_users->setName(UsersController::VIEW);
        $perm_users->setVisibleName("Просмотр пользователей");
        $perm_users->setDescription("Доступ на открытие и просмотр приложения");

        $perm_perm = new Permission();
        $perm_perm->setName(PermissionController::VIEW);
        $perm_perm->setVisibleName("Просмотр прав доступа");
        $perm_perm->setDescription("Доступ на открытие и просмотр приложения");

        $manager->persist($perm_anonymously);
        $manager->persist($perm_only_anonymously);
        $manager->persist($perm_roles);
        $manager->persist($perm_users);
        $manager->persist($perm_perm);

        $manager->flush();

    }

    public function getOrder()
    {
        return 1;
    }
}