<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;

class AppFixtures extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $encoder = new MessageDigestPasswordEncoder('sha512', true, 10);
        $role_admin = $this->getReference('role_admin');
        // Создаем пользователей
        $admin = new User();
        $admin->setName('Шахбоз');
        $admin->setSurname('Мирбадиев');
        $admin->setUsername('admin');
        $admin->setEmail('shahboz.100@yandex.ru');
        $admin->setSalt(md5(time()));
        $admin->setPassword($encoder->encodePassword('admin', $admin->getSalt()));
        $admin->addUserRole($role_admin);

        $manager->persist($admin);
        $manager->flush();
    }

    public function getOrder()
    {
        return 3;
    }
}
