<?php


namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class CatChar
 *
 * @ORM\Entity()
 * @ORM\Table(name="category_characteristic")
 */
class CatChar
{
    /**
     * @var int идентификатор роли
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id_catchar", type="integer", unique=true, options={"comment":"ИД категории"})
     */
    private $id;

    /**
     * @var Category
     *
     * @ORM\ManyToOne(targetEntity="Category")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id_category")
     */
    private $category;

    /**
     * @var Characteristic
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Characteristic")
     * @ORM\JoinColumn(name="caracteristic_id", referencedColumnName="id_characteristic")
     */
    private $characteristic;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param Category $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }

    /**
     * @return Characteristic
     */
    public function getCharacteristic()
    {
        return $this->characteristic;
    }

    /**
     * @param Characteristic $characteristic
     */
    public function setCharacteristic($characteristic)
    {
        $this->characteristic = $characteristic;
    }
}