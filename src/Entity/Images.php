<?php


namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * Class Images
 *
 * @ORM\Entity()
 * @ORM\Table(name="images")
 */
class Images
{
    /**
     * @var int идентификатор роли
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id_image", type="integer", unique=true, options={"comment":"ИД фото"})
     */
    private $id;

    /**
     * @var string Ссылка на фотографию
     *
     * @ORM\Column(name="link", type="string", length=250, unique=true, options={"comment":"Ссылка на фотографию"})
     */
    private $link;

    /**
     * @var Product
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Product", inversedBy="images")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id_product")
     */
    private $product;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @param string $link
     */
    public function setLink($link)
    {
        $this->link = $link;
    }

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param Product $product
     */
    public function setProduct($product)
    {
        $this->product = $product;
    }
}