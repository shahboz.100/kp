<?php


namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 * @ORM\Table(name="product")
 */
class Product
{
    /**
     * @var int идентификатор товара
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id_product", type="integer", unique=true, options={"comment":"ИД товара"})
     */
    private $id;

    /**
     * @var string название товара
     *
     * @ORM\Column(name="name", type="string", length=250, unique=true, options={"comment":"Название товара"})
     * @Assert\Length(
     *     min=3,
     *     max=250,
     *     minMessage="Должно быть не менее {{ limit }} символов",
     *     maxMessage="Должно быть не более {{ limit }} символов")
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @var Category
     *
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="products")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id_category")
     */
    private $category;

    /**
     * @var int Цена товара
     *
     * @ORM\Column(name="price", type="integer", options={"comment":"Цена"})
     */
    private $price;

    /**
     * @var Images
     * @ORM\OneToMany(targetEntity="App\Entity\Images", mappedBy="product")
     */
    private $images;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true, options={"comment":"Описание товара"})
     */
    private $description;

    /**
     * @var int
     *
     * @ORM\Column(name="count", type="integer", options={"comment":"Количество товара на складе"})
     */
    private $count;


    public function __construct()
    {
        $this->images = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param Category $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }

    /**
     * @return int
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param int $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return Images
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * @param Images $images
     */
    public function setImages($images)
    {
        $this->images = $images;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return int
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * @param int $count
     */
    public function setCount($count)
    {
        $this->count = $count;
    }


}