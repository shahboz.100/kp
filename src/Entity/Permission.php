<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Сущность прав доступа
 *
 *
 * Class Permission
 *
 * @package App\Entity
 *
 * @ORM\Entity()
 * @ORM\Table(name="permission", options={"comment":"Таблица прав доступа"})
 * @UniqueEntity("name", message="Введенное имя разрешения уже используется.")
 */
class Permission
{
    /**
     * @var int идентификатор права доступа
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id_permission", type="integer", unique=true, options={"comment":"Идентификатор права"})
     */
    private $id;

    /**
     * @var string Системное имя права доступа
     *
     * @ORM\Column(type="string", name="permission_name", length=100, unique=true, options={"comment":"Системное имя права
     *         доступа"})
     * @Assert\NotBlank(message="Поле не может быть пустым")
     */
    private $name;

    /**
     * @var string отображаемое имя права доступа
     *
     * @ORM\Column(type="string", length=100, options={"comment":"Короткое имя права доступа"})
     * @Assert\Length(max = 100, maxMessage = "Должно быть не более {{ limit }} символов")
     * @Assert\NotBlank(message="Поле не может быть пустым")
     */
    private $visibleName;

    /**
     * @var string Описание права доступа
     *
     * @ORM\Column(type="string", length=255, options={"comment"="Описание права доступа"})
     * @Assert\Length(max = 255, maxMessage = "Должно быть не более {{ limit }} символов")
     * @Assert\NotBlank(message="Поле не может быть пустым")
     */
    private $description;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="Role", mappedBy="permissions")
     */
    private $roles;

    public function __construct()
    {
        $this->roles = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->visibleName . ' (' . $this->name . ')';
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getVisibleName()
    {
        return $this->visibleName;
    }

    /**
     * @param string $visibleName
     */
    public function setVisibleName(string $visibleName)
    {
        $this->visibleName = $visibleName;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description)
    {
        $this->description = $description;
    }

    /**
     * @return Collection
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * Add role.
     *
     * @param Role $role
     *
     * @return Permission
     */
    public function addRole(Role $role)
    {
        $this->roles[] = $role;

        $role->addPermission($this);

        return $this;
    }

    /**
     * Remove role.
     *
     * @param Role $role
     */
    public function removeRole(Role $role)
    {
        $this->roles->removeElement($role);
        $role->removePermission($this);
    }
}
