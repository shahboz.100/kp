<?php


namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class ProdChar
 *
 * @ORM\Entity()
 * @ORM\Table(name="product_characteristic")
 */
class ProdChar
{
    /**
     * @var int идентификатор роли
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id_prodchar", type="integer", unique=true, options={"comment":"ИД продукта-характеристики"})
     */
    private $id;

    /**
     * @var Product
     *
     * @ORM\ManyToOne(targetEntity="Product")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id_product")
     */
    private $product;

    /**
     * @var CatChar
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\CatChar")
     * @ORM\JoinColumn(name="catchar_id", referencedColumnName="id_catchar")
     */
    private $category;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="string", length=250, options={"Значение"})
     */
    private $value;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param Product $product
     */
    public function setProduct($product)
    {
        $this->product = $product;
    }

    /**
     * @return CatChar
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param CatChar $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param string $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }
}