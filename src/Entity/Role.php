<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Role
 *
 * @package App\Entity
 *
 * @ORM\Entity()
 * @ORM\Table(name="roles", options={"comment":"Таблица ролей"})
 * @UniqueEntity("name", message="Введенное название роли уже используется.")
 */
class Role
{
    /**
     * @var int идентификатор роли
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id_role", type="integer", unique=true, options={"comment":"ИД роли"})
     */
    private $id;

    /**
     * @var string название роли
     *
     * @ORM\Column(name="name", type="string", length=12, unique=true, options={"comment":"Название роли"})
     * @Assert\Length(
     *     min=8,
     *     max=20,
     *     minMessage="Должно быть не менее {{ limit }} символов",
     *     maxMessage="Должно быть не более {{ limit }} символов")
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @var string Отображаемое имя
     *
     * @ORM\Column(name="visible_name", type="string", length=15, options={"default":"def","comment":"Отображаемое имя роли"})
     * @Assert\Length(
     *     min=3,
     *     max=15,
     *     minMessage="Должно быть не менее {{ limit }} символов",
     *     maxMessage="Должно быть не более {{ limit }} символов")
     * @Assert\NotBlank()
     */
    private $visibleName;

    /**
     * @var ArrayCollection Права доступа принадлежащие ролям
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Permission", inversedBy="roles")
     * @ORM\JoinTable(name="role_permissions",
     *  joinColumns={
     *      @ORM\JoinColumn(name="id_role", referencedColumnName="id_role")
     *  },
     *  inverseJoinColumns={
     *      @ORM\JoinColumn(name="id_permission", referencedColumnName="id_permission")
     *  }
     * )
     */
    private $permissions;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->permissions = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Role
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getVisibleName()
    {
        return $this->visibleName;
    }

    /**
     * @param string $visibleName
     */
    public function setVisibleName(string $visibleName)
    {
        $this->visibleName = $visibleName;
    }

    public function getRole()
    {
        return $this->getName();
    }

    public function __toString()
    {
        return $this->getName();
    }

    /**
     * @return Collection
     */
    public function getPermissions()
    {
        return $this->permissions;
    }

    /**
     * Returns the roles granted to the user.
     *
     * @return array (Permission|string)[] The user roles
     */
    public function getPermission()
    {
        $permissions = [];

        foreach ($this->getPermissions() as $item) {
            array_push($permissions, $item->getName());
        }

        return $permissions;
    }

    /**
     * Добавить право доступа
     *
     * @param Permission $permission
     *
     * @return Role|array
     */
    public function addPermission(Permission $permission)
    {
        $this->permissions[] = $permission;

        return $this;
    }

    /**
     * Удалить право доступа
     *
     * @param Permission $permission
     */
    public function removePermission(Permission $permission)
    {
        $this->permissions->removeElement($permission);
    }
}
