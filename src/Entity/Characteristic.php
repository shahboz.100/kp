<?php


namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity()
 * @ORM\Table(name="characteristic")
 */
class Characteristic
{
    /**
     * @var int идентификатор роли
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id_characteristic", type="integer", unique=true, options={"comment":"ИД характеристики"})
     */
    private $id;

    /**
     * @var string название характеристики
     *
     * @ORM\Column(name="name", type="string", length=250, unique=true, options={"comment":"Название характеристики"})
     * @Assert\Length(
     *     min=3,
     *     max=250,
     *     minMessage="Должно быть не менее {{ limit }} символов",
     *     maxMessage="Должно быть не более {{ limit }} символов")
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * One Category has Many Categories.
     * @ORM\OneToMany(targetEntity="App\Entity\Characteristic", mappedBy="parent")
     */
    private $children;

    /**
     * Many Categories have One Category.
     * @ORM\ManyToOne(targetEntity="App\Entity\Characteristic", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id_characteristic")
     */
    private $parent;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param mixed $children
     */
    public function setChildren($children)
    {
        $this->children = $children;
    }

    /**
     * @return mixed
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param mixed $parent
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
    }

}