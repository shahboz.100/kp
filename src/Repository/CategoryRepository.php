<?php


namespace App\Repository;


use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;

class CategoryRepository extends EntityRepository
{
    public function getChars($category)
    {
        $rsm = new ResultSetMapping();
        $rsm->addScalarResult('id_catchar', 'id');
        $rsm->addScalarResult('name', 'name');

        $sql = 'select cc.id_catchar, c.name from category_characteristic cc 
                join characteristic c 
                    on cc.caracteristic_id = c.id_characteristic 
                where cc.category_id = :category_id';

        $query = $this->getEntityManager()->createNativeQuery($sql, $rsm);
        $query->setParameter('category_id', $category);

        return $query->getResult();
    }
}