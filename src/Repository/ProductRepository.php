<?php


namespace App\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;


class ProductRepository extends EntityRepository
{

    public function getCharacteristicsForEdit($product)
    {
        $rsm = new ResultSetMapping();
        $rsm->addScalarResult('id_catchar', 'id_catchar');
        $rsm->addScalarResult('parent_id', 'parent_id');
        $rsm->addScalarResult('name', 'name');
        $rsm->addScalarResult('value', 'value');

        $query = $this->getEntityManager()->createNativeQuery('select cc.id_catchar, c.name, pc.value from product_characteristic pc
    join category_characteristic cc on pc.catchar_id = cc.id_catchar
    join characteristic c on cc.caracteristic_id = c.id_characteristic where pc.product_id = :product_id', $rsm);
        $query->setParameter('product_id', $product);

        return $query->getResult();
    }

    public function getCharacteristics($product, $parent)
    {
        $rsm = new ResultSetMapping();
        $rsm->addScalarResult('id_catchar', 'id_catchar');
        $rsm->addScalarResult('parent_id', 'parent_id');
        $rsm->addScalarResult('name', 'name');
        $rsm->addScalarResult('value', 'value');

        $query = $this->getEntityManager()->createNativeQuery('select cc.id_catchar, c.name, c.parent_id, pc.value from product_characteristic pc
        join category_characteristic cc on pc.catchar_id = cc.id_catchar
        join characteristic c on cc.caracteristic_id = c.id_characteristic where pc.product_id = :product_id and c.parent_id = :parent_id', $rsm);
        $query->setParameter('product_id', $product);
        $query->setParameter('parent_id', $parent);

        return $query->getResult();
    }

    public function getParentChars($product)
    {
        $rsm = new ResultSetMapping();
        $rsm->addScalarResult('parent_id', 'parent_id');
        $rsm->addScalarResult('name', 'name');

        $query = $this->getEntityManager()->createNativeQuery('select t.parent_id, (select p.name from characteristic p where p.id_characteristic = t.parent_id) from (select c.parent_id
from product_characteristic pc
         join category_characteristic cc on pc.catchar_id = cc.id_catchar
         join characteristic c on cc.caracteristic_id = c.id_characteristic
where pc.product_id = :product_id

group by c.parent_id) t', $rsm);
        $query->setParameter('product_id', $product);

        return $query->getResult();
    }


}