<?php
/**
 * Класс - voter отвечающий за проверку прав доступа
 *
 * @date 20.04.2018
 * @author <shahboz.100@yandex.ru>
 */

namespace App\Security;

use App\Entity\Permission;
use App\Entity\Role;
use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class AccessVoter extends Voter {

    private $decisionManager;

    public function __construct(AccessDecisionManagerInterface $decisionManager) {
        $this->decisionManager = $decisionManager;
    }

    protected function supports($attribute, $subject) {
        if ('IS_AUTHENTICATED_' === substr($attribute, 0, 17) || 'ROLE_' === substr($attribute, 0, 5)) {
            return false;
        }
        return true;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token) {
        $user = $token->getUser();

        if (!$user instanceof User) {
            return false;
        }

        if ($this->decisionManager->decide($token, array('ROLE_ADMIN'))) {
            return true;
        }

        /**
         * Формирование массива системных имен разрешений текущего пользователя
         *
         * @var array $permissions
         * @var Role $role
         * @var Permission $permission
         */
        $permissions = [];
        foreach ($user->getUserRoles() as $role) {
            foreach ($role->getPermissions() as $permission) {
                $permissions[] = $permission->getName();
            }
        }
        /**
         * Проверка существования атрибута в массиве системных имен разрешений
         */
        if ($permissions && in_array($attribute, $permissions)) {
            return true;
        }

        return false;
    }
}