<?php


namespace App\Controller;

use App\Entity\CatChar;
use App\Form\CatCharType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CatCharController
 *
 * @Route("/catchar", name="carchar_")
 */
class CatCharController extends AbstractController
{
    const MODULE_NAME = "CATCHAR";
    const VIEW = self::MODULE_NAME . '_VIEW';
    const CREATE = self::MODULE_NAME . "_CREATE";
    const UPDATE = self::MODULE_NAME . "_UPDATE";
    const DELETE = self::MODULE_NAME . '_DELETE';

    /**
     * @Route("/", name="index")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $this->denyAccessUnlessGranted(self::VIEW);

        $catchars = $this->getDoctrine()->getRepository(CatChar::class)->findAll();

        return $this->render('admin/catchar/index.html.twig', [
            'catchars' => $catchars
        ]);
    }

    /**
     * @Route("/add/", name="add")
     * @Route("/edit/{id}", requirements={"id":"\d+"}, name="edit")
     *
     * @param Request $request
     * @param         $id
     *
     * @return Response
     */
    public function editAction(Request $request, $id = null)
    {
        if ($id) {
            $this->denyAccessUnlessGranted(self::UPDATE);
            $catchar = $this->getDoctrine()->getRepository(CatChar::class)->find($id);
            if (!$catchar) {
                throw new NotFoundHttpException("Нет такой записи!");
            }
        } else {
            $this->denyAccessUnlessGranted(self::CREATE);
            $catchar = new CatChar();
        }

        $form = $this->createForm(CatCharType::class, $catchar);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if (!$id) {
                $this->getDoctrine()->getManager()->persist($catchar);
            }
            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('carchar_index');
        }

        return $this->render('admin/catchar/edit.html.twig', [
            'form' => $form->createView(),
            'title' => $id ? 'Редактирование записи' : 'Добавление записи'
        ]);
    }


    /**
     * Удаление роли
     *
     * @Route("/delete/{id}", name="delete", requirements={"id"="\d+"})
     *
     * @param Request $request
     * @param null    $id
     *
     * @return RedirectResponse
     */
    public function deleteAction(Request $request, $id = null)
    {
        $this->denyAccessUnlessGranted(self::DELETE);

        $catchar = $this->getDoctrine()->getRepository(CatChar::class)->find($id);

        if (!$catchar) {
            throw new NotFoundHttpException('Нет такой записи!');
        }

        $entityManager = $this->getDoctrine()->getManager();

        $entityManager->remove($catchar);
        $entityManager->flush();

        return $this->redirectToRoute('carchar_index');
    }
}