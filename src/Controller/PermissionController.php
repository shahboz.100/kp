<?php


namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class PermissionController extends AbstractController
{
    const MODULE_NAME = 'PERMISSIONS';

    const VIEW = self::MODULE_NAME . '_VIEW';
    const CREATE = self::MODULE_NAME . '_CREATE';
    const UPDATE = self::MODULE_NAME . '_UPDATE';
    const DELETE = self::MODULE_NAME . '_DELETE';

    public function indexAction(Request $request)
    {

    }


}