<?php


namespace App\Controller;


use App\Entity\Category;
use App\Form\CategoryType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CategoryController
 *
 * @Route("/category", name="category_")
 */
class CategoryController extends AbstractController
{
    const MODULE_NAME = "CATEGORY";
    const VIEW = self::MODULE_NAME . '_VIEW';
    const CREATE = self::MODULE_NAME . "_CREATE";
    const UPDATE = self::MODULE_NAME . "_UPDATE";
    const DELETE = self::MODULE_NAME . '_DELETE';

    /**
     * @Route("/", name="index")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $this->denyAccessUnlessGranted(self::VIEW);

        $categories = $this->getDoctrine()->getRepository(Category::class)->findAll();

        return $this->render('admin/category/index.html.twig', [
            'categories' => $categories
        ]);
    }

    /**
     * @Route("/add/", name="add")
     * @Route("/edit/{id}", requirements={"id":"\d+"}, name="edit")
     *
     * @param Request $request
     * @param         $id
     *
     * @return Response
     */
    public function editAction(Request $request, $id = null)
    {
        if ($id) {
            $this->denyAccessUnlessGranted(self::UPDATE);
            $char = $this->getDoctrine()->getRepository(Category::class)->find($id);
            if (!$char) {
                throw new NotFoundHttpException("Нет такой записи!");
            }
        } else {
            $this->denyAccessUnlessGranted(self::CREATE);
            $char = new Category();
        }

        $form = $this->createForm(CategoryType::class, $char);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if (!$id) {
                $this->getDoctrine()->getManager()->persist($char);
            }
            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('category_index');
        }

        return $this->render('admin/category/edit.html.twig', [
            'form' => $form->createView(),
            'title' => $id ? 'Редактирование категории' : 'Добавление категории'
        ]);
    }


    /**
     * Удаление роли
     *
     * @Route("/delete/{id}", name="delete", requirements={"id"="\d+"})
     *
     * @param Request $request
     * @param null    $id
     *
     * @return RedirectResponse
     */
    public function deleteAction(Request $request, $id = null)
    {
        $this->denyAccessUnlessGranted(self::DELETE);

        $category = $this->getDoctrine()->getRepository(Category::class)->find($id);

        if (!$category) {
            throw new NotFoundHttpException('Нет такой записи!');
        }

        $entityManager = $this->getDoctrine()->getManager();

        $entityManager->remove($category);
        $entityManager->flush();

        return $this->redirectToRoute('category_index');
    }
}