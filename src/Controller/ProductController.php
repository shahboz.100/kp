<?php


namespace App\Controller;


use App\Entity\CatChar;
use App\Entity\Category;
use App\Entity\Images;
use App\Entity\ProdChar;
use App\Entity\Product;
use App\Form\ProductType;
use App\Services\ImageUploader;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ProductController
 *
 * @Route("/product", name="product_")
 */
class ProductController extends AbstractController
{
    const MODULE_NAME = "PRODUCTS";
    const VIEW = self::MODULE_NAME . '_VIEW';
    const CREATE = self::MODULE_NAME . "_CREATE";
    const UPDATE = self::MODULE_NAME . "_UPDATE";
    const DELETE = self::MODULE_NAME . '_DELETE';

    /**
     * @Route("/", name="index")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $this->denyAccessUnlessGranted(self::VIEW);

        $products = $this->getDoctrine()->getRepository(Product::class)->findAll();

        return $this->render('admin/product/index.html.twig', [
            'products' => $products
        ]);
    }

    /**
     * @Route("/show/{id}", requirements={"id":"\d+"}, name="show")
     *
     * @param Request $request
     *
     * @param         $id
     *
     * @return Response
     */
    public function showAction(Request $request, $id)
    {
        $this->denyAccessUnlessGranted(self::VIEW);

        $product = $this->getDoctrine()->getRepository(Product::class)->find($id);

        $parents = $this->getDoctrine()->getRepository(Product::class)->getParentChars($product->getId());
        $res = [];
        foreach ($parents as $parent){
            $r = [];
            $r['parent'] = $parent;
            $r['chars'] = $this->getDoctrine()->getRepository(Product::class)->getCharacteristics($product->getId(), $parent['parent_id']);
            array_push($res, $r);
        }

        if (!$product) {
            throw new NotFoundHttpException('Товар с данным идентификатором не найден!');
        }

        return $this->render('admin/product/show.html.twig', [
            'product' => $product,
            'characteristics' => $res
        ]);
    }

    /**
     * @Route("/add/", name="add")
     *
     * @param Request       $request
     * @param ImageUploader $imageUploader
     *
     * @return Response
     */
    public function addAction(Request $request, ImageUploader $imageUploader)
    {

        $this->denyAccessUnlessGranted(self::CREATE);

        $form = $this->createForm(ProductType::class, null, ['em' => $this->getDoctrine()->getManager()]);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $product = new Product();
            $images = $form->get('images')->getData();

            foreach ($images as $file) {
                $filename = $imageUploader->upload($file);
                $image = new Images();
                $image->setProduct($product);
                $image->setLink($filename);
                $this->getDoctrine()->getManager()->persist($image);
            }

            $product->setName($form->get('name')->getData());
            $product->setCategory($form->get('category')->getData());
            $product->setPrice($form->get('price')->getData());
            $product->setCount($form->get('count')->getData());
            $product->setDescription($form->get('description')->getData());
            $this->getDoctrine()->getManager()->persist($product);
            $characteristics = $form->get('characteristic')->getData();

            foreach ($characteristics as $characteristic) {
                $catchar = $this->getDoctrine()->getRepository(CatChar::class)->find($characteristic['category']);
                $prodChar = new ProdChar();
                $prodChar->setProduct($product);
                $prodChar->setCategory($catchar);
                $prodChar->setValue($characteristic['value']);
                $this->getDoctrine()->getManager()->persist($prodChar);
            }

            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('product_index');
        }

        return $this->render('admin/product/edit.html.twig', [
            'form' => $form->createView(),
            'title' => 'Добавление товара'
        ]);
    }

    /**
     * @Route("/edit/{id}", requirements={"id":"\d+"}, name="edit")
     *
     * @param Request $request
     * @param         $id
     *
     * @return Response
     */
    public function editAction(Request $request, $id = null)
    {
        $this->denyAccessUnlessGranted(self::UPDATE);
        $product = $this->getDoctrine()->getRepository(Product::class)->find($id);

        if (!$product) {
            throw new NotFoundHttpException("Нет такой записи!");
        }

        $originalChars = $this->getDoctrine()->getRepository(Product::class)->getCharacteristicsForEdit($product->getId());

        $form = $this->createForm(ProductType::class, $product, [
            'em' => $this->getDoctrine()->getManager(),
            'characteristics' => $originalChars
        ]);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $chars = $form->get('characteristic')->getData();

            /** @var ProdChar $item */
            foreach ($chars as $item) {
                $catchar = $this->getDoctrine()->getRepository(CatChar::class)->find($item['category']);

                if (!$this->inArr($originalChars, $item)) {
                    $prodChar = new ProdChar();
                    $prodChar->setProduct($product);
                    $prodChar->setCategory($catchar);
                    $prodChar->setValue($item['value']);
                    $this->getDoctrine()->getManager()->persist($prodChar);
                    $this->getDoctrine()->getManager()->flush();
                } else {
                    $prodChar = $this->getDoctrine()->getRepository(ProdChar::class)->findOneBy([
                        'product' => $product,
                        'category' => $item['category']
                    ]);
                    $prodChar->setValue($item['value']);
                    $this->getDoctrine()->getManager()->flush();
                }
            }

            foreach ($originalChars as $item) {
                if (!$this->inArr($chars, $item)) {
                    $prodChar = $this->getDoctrine()->getRepository(ProdChar::class)->findOneBy([
                        'product' => $product,
                        'category' => $item['id_catchar']
                    ]);
                    if ($prodChar) {
                        $this->getDoctrine()->getManager()->remove($prodChar);
                        $this->getDoctrine()->getManager()->flush();
                    }
                }
            }


            return $this->redirectToRoute('product_index');
        }

        return $this->render('admin/product/edit.html.twig', [
            'form' => $form->createView(),
            'title' => 'Редактирование товара'
        ]);
    }

    /**
     * @param          $arr
     * @param ProdChar $v
     *
     * @return bool
     */
    private function inArr($arr, $v)
    {
        if (!isset($v['id_catchar'])) {
            return false;
        }

        /** @var ProdChar $item */
        foreach ($arr as $item) {
            if ($item['id_catchar'] == $v['id_catchar']) {
                return true;
            }
        }
        return false;
    }

    /**
     * Удаление роли
     *
     * @Route("/delete/{id}", name="delete", requirements={"id"="\d+"})
     *
     * @param Request $request
     * @param null    $id
     *
     * @return RedirectResponse
     */
    public function deleteAction(Request $request, $id = null)
    {
        $this->denyAccessUnlessGranted(self::DELETE);

        $product = $this->getDoctrine()->getRepository(Product::class)->find($id);

        if (!$product) {
            throw new NotFoundHttpException('Нет такой записи!');
        }

        $entityManager = $this->getDoctrine()->getManager();

        $entityManager->remove($product);
        $entityManager->flush();

        return $this->redirectToRoute('product_index');
    }

    /**
     * @Route("/getchar/{id}", requirements={"id":"\d+"}, name="getchar")
     * @param Request $request
     * @param         $id
     *
     * @return mixed
     */
    public function getChar(Request $request, $id)
    {
        $this->denyAccessUnlessGranted(self::CREATE);

        $chars = $this->getDoctrine()->getRepository(Category::class)->getChars($id);

        return new JsonResponse($chars);
    }
}