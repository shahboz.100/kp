<?php


namespace App\Controller;

use App\Entity\Role;
use App\Form\RoleType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/role", name="role_")
 */
class RolesController extends AbstractController
{
    const MODULE_NAME = "ROLES";
    const VIEW = self::MODULE_NAME . '_VIEW';
    const CREATE = self::MODULE_NAME . "_CREATE";
    const UPDATE = self::MODULE_NAME . "_UPDATE";
    const DELETE = self::MODULE_NAME . '_DELETE';

    /**
     * @Route("/", name="index")
     *
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $this->denyAccessUnlessGranted(self::VIEW);

        $roles = $this->getDoctrine()->getRepository(Role::class)->findAll();

        return $this->render('admin/role/roleIndex.html.twig', [
            'roles' => $roles
        ]);
    }

    /**
     * @Route("/add/", name="add")
     * @Route("/edit/{id}", requirements={"id":"\d+"}, name="edit")
     *
     * @param Request $request
     * @param $id
     * @return Response
     */
    public function editAction(Request $request, $id = null)
    {
        if ($id) {
            $this->denyAccessUnlessGranted(self::UPDATE);
            $role = $this->getDoctrine()->getRepository(Role::class)->find($id);
            if (!$role) {
                throw new NotFoundHttpException("Нет такой записи!");
            }
        } else {
            $this->denyAccessUnlessGranted(self::CREATE);
            $role = new Role();
        }

        $form = $this->createForm(RoleType::class, $role);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if (!$id) {
                $this->getDoctrine()->getManager()->persist($role);
            }
            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('role_index');
        }

        return $this->render('admin/role/edit.html.twig', [
            'form' => $form->createView(),
            'title' => $id ? 'Редактирование роли' : 'Добавление роли'
        ]);
    }


    /**
     * Удаление роли
     *
     * @Route("/delete/{id}", name="delete", requirements={"id"="\d+"})
     *
     * @param Request $request
     * @param null $id
     * @return RedirectResponse
     */
    public function deleteAction(Request $request, $id = null)
    {
        $this->denyAccessUnlessGranted(self::DELETE);

        $role = $this->getDoctrine()->getRepository(Role::class)->find($id);

        if (!$role){
            throw new NotFoundHttpException('Нет такой записи!');
        }

        $entityManager = $this->getDoctrine()->getManager();

        $entityManager->remove($role);
        $entityManager->flush();

        return $this->redirectToRoute('role_index');
    }
}