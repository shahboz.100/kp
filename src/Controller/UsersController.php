<?php


namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class UsersController extends AbstractController
{
    const MODULE_NAME = "USERS";

    const VIEW = self::MODULE_NAME . '_VIEW';
    const CREATE = self::MODULE_NAME . '_CREATE';
    const UPDATE = self::MODULE_NAME . '_UPDATE';
    const DELETE = self::MODULE_NAME . '_DELETE';



}