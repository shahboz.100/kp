<?php


namespace App\Controller;

use App\Entity\Category;
use App\Entity\Product;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{
    /**
     * @Route("/", name="index")
     * @param Request $request
     */
    public function indexAction(Request $request)
    {
        $categories = $this->getDoctrine()->getRepository(Category::class)->findAll();


        return $this->render('index.html.twig', [
            'categories' => $categories
        ]);
    }

    /**
     * @Route("/show/{id}", requirements={"id":"\d+"}, name="show")
     *
     * @param Request $request
     *
     * @param         $id
     *
     * @return Response
     */
    public function showAction(Request $request, $id)
    {
        $product = $this->getDoctrine()->getRepository(Product::class)->find($id);

        $parents = $this->getDoctrine()->getRepository(Product::class)->getParentChars($product->getId());
        $res = [];
        foreach ($parents as $parent){
            $r = [];
            $r['parent'] = $parent;
            $r['chars'] = $this->getDoctrine()->getRepository(Product::class)->getCharacteristics($product->getId(), $parent['parent_id']);
            array_push($res, $r);
        }

        if (!$product) {
            throw new NotFoundHttpException('Товар с данным идентификатором не найден!');
        }

        return $this->render('admin/product/show.html.twig', [
            'product' => $product,
            'characteristics' => $res
        ]);
    }

    /**
     * @Route("/categories", name="categories")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function categoriesAction(Request $request)
    {
        $categories = $this->getDoctrine()->getRepository(Category::class)->findAll();

        return $this->render('categoryIndex.html.twig', [
            'categories' => $categories
        ]);
    }

    /**
     * @Route("/category/{id}", requirements={"id":"\d+"}, name="category_show")
     *
     * @param Request $request
     *
     * @param         $id
     *
     * @return Response
     */
    public function categoryAction(Request $request, $id)
    {
        $category = $this->getDoctrine()->getRepository(Category::class)->find($id);

        if (!$category) {
            throw new NotFoundHttpException('Категория с данным идентификатором не найдена!');
        }

        return $this->render('category.html.twig', [
            'category' => $category,
        ]);
    }
}