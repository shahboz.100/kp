<?php


namespace App\Controller;


use App\Entity\Characteristic;
use App\Form\CharType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/characteristik", name="char_")
 */
class CharacteristicController extends AbstractController
{
    const MODULE_NAME = "CHARACTERISTIK";
    const VIEW = self::MODULE_NAME . '_VIEW';
    const CREATE = self::MODULE_NAME . "_CREATE";
    const UPDATE = self::MODULE_NAME . "_UPDATE";
    const DELETE = self::MODULE_NAME . '_DELETE';

    /**
     * @Route("/", name="index")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $this->denyAccessUnlessGranted(self::VIEW);

        $chars = $this->getDoctrine()->getRepository(Characteristic::class)->findAll();

        return $this->render('admin/characteristik/index.html.twig', [
            'chars' => $chars
        ]);
    }

    /**
     * @Route("/add/", name="add")
     * @Route("/edit/{id}", requirements={"id":"\d+"}, name="edit")
     *
     * @param Request $request
     * @param         $id
     *
     * @return Response
     */
    public function editAction(Request $request, $id = null)
    {
        if ($id) {
            $this->denyAccessUnlessGranted(self::UPDATE);
            $char = $this->getDoctrine()->getRepository(Characteristic::class)->find($id);
            if (!$char) {
                throw new NotFoundHttpException("Нет такой записи!");
            }
        } else {
            $this->denyAccessUnlessGranted(self::CREATE);
            $char = new Characteristic();
        }

        $form = $this->createForm(CharType::class, $char);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if (!$id) {
                $this->getDoctrine()->getManager()->persist($char);
            }
            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('char_index');
        }

        return $this->render('admin/characteristik/edit.html.twig', [
            'form' => $form->createView(),
            'title' => $id ? 'Редактирование характеристики' : 'Добавление характеристики'
        ]);
    }


    /**
     * Удаление роли
     *
     * @Route("/delete/{id}", name="delete", requirements={"id"="\d+"})
     *
     * @param Request $request
     * @param null    $id
     *
     * @return RedirectResponse
     */
    public function deleteAction(Request $request, $id = null)
    {
        $this->denyAccessUnlessGranted(self::DELETE);

        $role = $this->getDoctrine()->getRepository(Characteristic::class)->find($id);

        if (!$role) {
            throw new NotFoundHttpException('Нет такой записи!');
        }

        $entityManager = $this->getDoctrine()->getManager();

        $entityManager->remove($role);
        $entityManager->flush();

        return $this->redirectToRoute('char_index');
    }
}